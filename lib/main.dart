import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Create Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Container(
          decoration: BoxDecoration(
           gradient: new LinearGradient(
              colors: [
                const Color(0xFF1CD8D2),
                const Color(0xFF93EDC7),
              ],
             begin: Alignment.centerRight,
              end: new Alignment(-1.0, -1.0),
           ),
            image: DecorationImage(
              image: AssetImage('images/background1.png'),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                  Colors.green[700].withOpacity(0.5), BlendMode.dstATop),
            ),
          ),
          child: SafeArea(
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              children: <Widget>[
                SizedBox(height: 90.0),
                Text(
                  "Welcome to",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30.0,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 40.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "images/logo.png",
                      height: 150.0,
                      color: Colors.white,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 4.0, right: 30.0),
                      child: Text(
                        "AgroSight",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 60.0,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 50.0),
                ButtonTheme(
                  buttonColor: Colors.white,
                  height: 80.0,
                  minWidth: 200.0,
                  child: RaisedButton(
                    onPressed: () {},
                    child: Text(
                      "Login with facebook",
                      style: TextStyle(color: Colors.blue, fontSize: 24.0),
                    ),
                  ),
                ),
                SizedBox(height: 60.0),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Email",
                    labelStyle: TextStyle(
                      fontSize: 25.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: "Password",
                    labelStyle: TextStyle(
                      fontSize: 25.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(height: 40.0),
                ButtonTheme(
                  buttonColor: Colors.white,
                  height: 80.0,
                  minWidth: 600.0,
                  child: RaisedButton(
                    onPressed: () {
                    
                    },
                    child: Text(
                      "Login ",
                      style: TextStyle(color: Colors.black, fontSize: 24.0),
                    ),
                  ),
                ),
                SizedBox(height: 80.0),
                 Padding(
                   padding: const EdgeInsets.all(40.0),
                   child: Container(
                    child: Row(
                       crossAxisAlignment: CrossAxisAlignment.center,
                       mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.only(right: 5.0),
                          child: Text(
                            'Dont have account?',
                            style: TextStyle(
                                fontSize: 25.0, color: Colors.white),
                          ),
                        ),
                        Text(
                          'REGISTER NOW',
                          style: TextStyle(
                              fontSize: 25.0, color: Colors.yellow[300]),
                        ),
                      ],
                    ),
                ),
                 )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
